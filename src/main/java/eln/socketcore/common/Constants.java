package eln.socketcore.common;

public class Constants {
    public static final String SESSION_USER = "SESSION_USER";
    public static final String SESSION_LOGIN_REDIRECT_URL = "LOGIN_REDIRECT_URL";
    public static final String REDIS_UNREAD_MSG_PREFIX = "stomp-websocket:unread_msg:";

}
