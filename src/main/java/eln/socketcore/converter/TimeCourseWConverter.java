package eln.socketcore.converter;

import eln.common.core.entities.TimeCourseWare;
import eln.socketcore.dto.TimeCourseWareDTO;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class TimeCourseWConverter implements IDTO<TimeCourseWareDTO>, IEntity<TimeCourseWare>{

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public TimeCourseWareDTO convertToDTO(Object entity) {
        TimeCourseWare timeCourseWare = (TimeCourseWare) entity;
        TimeCourseWareDTO timeCourseWareDTO = modelMapper.map(timeCourseWare, TimeCourseWareDTO.class);
        return timeCourseWareDTO;
    }

    @Override
    public TimeCourseWare convertToEntity(Object dto) {
        TimeCourseWareDTO courseWareDTO = (TimeCourseWareDTO) dto;
        TimeCourseWare timeCourseWare =  modelMapper.map(courseWareDTO, TimeCourseWare.class);
        return timeCourseWare;
    }
}
