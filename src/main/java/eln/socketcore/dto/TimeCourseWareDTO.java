package eln.socketcore.dto;


import java.util.Date;

public class TimeCourseWareDTO {

    private Long id;
    private Date timeConnect;
    private Date timeDisconnect;
    private Long idCourseware;
    private Long idChapter;

    private String token;



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getTimeConnect() {
        return timeConnect;
    }

    public void setTimeConnect(Date timeConnect) {
        this.timeConnect = timeConnect;
    }

    public Date getTimeDisconnect() {
        return timeDisconnect;
    }

    public void setTimeDisconnect(Date timeDisconnect) {
        this.timeDisconnect = timeDisconnect;
    }

    public Long getIdCourseware() {
        return idCourseware;
    }

    public void setIdCourseware(Long idCourseware) {
        this.idCourseware = idCourseware;
    }

    public Long getIdChapter() {
        return idChapter;
    }

    public void setIdChapter(Long idChapter) {
        this.idChapter = idChapter;
    }


    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}