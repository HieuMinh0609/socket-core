package eln.socketcore.api;


import eln.socketcore.dto.TimeCourseWareDTO;
import eln.socketcore.service.TimeCourseWareService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;


@Controller
public class TimeCourseWareApi {
    @Autowired
    private TimeCourseWareService timeCourseWareService;

    @MessageMapping("/course-ware/save")
    @SendTo("/topic/test")
    public void setTimeCourse(TimeCourseWareDTO message) throws Exception {
        timeCourseWareService.saveTime(message);
    }
}
