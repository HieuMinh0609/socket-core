package eln.socketcore.api;

import eln.socketcore.common.Constants;
import eln.socketcore.dto.UserDTO;
import eln.socketcore.utils.AesUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/time-math")
public class UserApi {
    @Autowired
    private AesUtils aesUtils;

    @PostMapping("/user/save-session")
    public ResponseEntity<String> setSession(@RequestBody UserDTO userDTO, HttpServletRequest request) {
       String encrypt =  aesUtils.encryptCbcMode(userDTO.getUsername(),null,null);
        HttpSession session = request.getSession();
        session.setAttribute(Constants.SESSION_USER, userDTO.getUsername());
        return ResponseEntity.ok(encrypt);

    }
}
