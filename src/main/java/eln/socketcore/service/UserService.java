package eln.socketcore.service;

import eln.common.core.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    public Boolean checkUsername(String username) {
        try {
            if (userRepository.findByUsernameAndStatus(username, 1) != null) {
                return true;
            }
        } catch (Exception e) {
            return false;
        }
        return false;
    }
}
