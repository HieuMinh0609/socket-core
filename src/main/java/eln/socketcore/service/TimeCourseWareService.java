package eln.socketcore.service;

import eln.common.core.repository.TimeCourseWareRepository;
import eln.socketcore.converter.TimeCourseWConverter;
import eln.socketcore.dto.TimeCourseWareDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TimeCourseWareService {
    @Autowired
    private TimeCourseWareRepository timeCourseWareRepository;
    @Autowired
    private TimeCourseWConverter converter;

    public void saveTime(TimeCourseWareDTO timeCourseWareDTO){
        timeCourseWareRepository.save(converter.convertToEntity(timeCourseWareDTO));
    }

}
