package eln.socketcore.interceptor;

import eln.common.core.entities.User;
import eln.socketcore.common.Constants;
import eln.socketcore.common.SpringContextUtils;
import eln.socketcore.service.UserService;
import eln.socketcore.utils.AesUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LoginInterceptor extends HandlerInterceptorAdapter {
    @Autowired
    private UserService userService;
    @Autowired
    private AesUtils aesUtils;



    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
      String  string =   request.getHeaders("jwt-token")+"";
        String token = request.getParameter("eln_secretKey");
        if(userService.checkUsername(aesUtils.decryptCbcMode(token,null,null))){
            return true;
        }else{
            return false;
        }
    }
}