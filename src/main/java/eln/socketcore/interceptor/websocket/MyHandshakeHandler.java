package eln.socketcore.interceptor.websocket;

import eln.common.core.entities.User;
import eln.socketcore.common.Constants;
import eln.socketcore.common.SpringContextUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.server.support.DefaultHandshakeHandler;

import javax.servlet.http.HttpSession;
import java.security.Principal;
import java.text.MessageFormat;
import java.util.Map;

@Component
public class MyHandshakeHandler extends DefaultHandshakeHandler {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    protected Principal determineUser(ServerHttpRequest request, WebSocketHandler wsHandler, Map<String, Object> attributes) {
//        HttpSession session = SpringContextUtils.getSession();
//        String username = (String) session.getAttribute(Constants.SESSION_USER);
        String username ="admin";
        if(username != null){
            logger.debug(MessageFormat.format("WebSocket Principal：{0}", username));
            return new MyPrincipal(username);
        }else{
            logger.error("WebSocket");
            return null;
        }
    }

}