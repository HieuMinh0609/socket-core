package eln.socketcore.interceptor.websocket;

import eln.common.core.entities.User;
import eln.socketcore.common.Constants;
import eln.socketcore.common.SpringContextUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.server.HandshakeInterceptor;

import javax.servlet.http.HttpSession;
import java.text.MessageFormat;
import java.util.Map;

@Component
public class AuthHandshakeInterceptor implements HandshakeInterceptor {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public boolean beforeHandshake(ServerHttpRequest serverHttpRequest, ServerHttpResponse serverHttpResponse, WebSocketHandler webSocketHandler, Map<String, Object> map) throws Exception {
//        HttpSession session = SpringContextUtils.getSession();
//       String username = (String) session.getAttribute(Constants.SESSION_USER);
        String username ="admin";
        HttpHeaders headers = serverHttpRequest.getHeaders();
        HttpHeaders heasders =serverHttpResponse.getHeaders();
        HttpSession session = SpringContextUtils.getSession();
        String usernames =  session.getAttribute(Constants.SESSION_USER)+"";

        if(username != null){
            logger.debug(MessageFormat.format("Success WebSocket",username));
            return true;
        }else{
            logger.error("Error WebSocket");
            return false;
        }

    }

    @Override
    public void afterHandshake(ServerHttpRequest serverHttpRequest, ServerHttpResponse serverHttpResponse, WebSocketHandler webSocketHandler, Exception e) {

    }

}
