package eln.socketcore.interceptor.websocket;

import eln.socketcore.service.UserService;
import eln.socketcore.utils.AesUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.core.tools.picocli.CommandLine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.ChannelInterceptor;
import org.springframework.messaging.support.MessageHeaderAccessor;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketHandler;

import javax.servlet.http.HttpSession;
import java.security.Principal;
import java.text.MessageFormat;
import java.util.Map;

@Component
public class MyChannelInterceptor  implements ChannelInterceptor {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    private UserService userService;
    @Autowired
    private AesUtils aesUtils;

    @Override
    public void afterSendCompletion(Message<?> message, MessageChannel channel, boolean sent, Exception ex) {
        StompHeaderAccessor accessor = StompHeaderAccessor.wrap(message);
        StompCommand command = accessor.getCommand();


        if(StompCommand.DISCONNECT.equals(command)){
            String user = "";
            Principal principal = accessor.getUser();
            if(principal != null && StringUtils.isNoneBlank(principal.getName())){
                user = principal.getName();
            }else{
                user = accessor.getSessionId();
            }

            logger.debug(MessageFormat.format("Disconnect WebSocket", user));
        }else {
            try {
                StompHeaderAccessor accessors
                        = MessageHeaderAccessor.getAccessor(message, StompHeaderAccessor.class);
                String headervalue= accessors.getNativeHeader("jwt-token").get(0);


                if (StompCommand.CONNECT.equals(accessor.getCommand())) {
                    String name =  aesUtils.decryptCbcMode(headervalue,null,null);
                    if(! userService.checkUsername(aesUtils.decryptCbcMode(headervalue,null,null))){
                        logger.debug(MessageFormat.format("Disconnect WebSocket", ""));
                    }
                }
            }catch (Exception e){
                logger.debug(MessageFormat.format("Disconnect WebSocket",e.getLocalizedMessage()));
                throw e;
            }
        }
    }



}
