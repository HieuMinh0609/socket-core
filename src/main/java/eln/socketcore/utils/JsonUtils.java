package eln.socketcore.utils;

import com.alibaba.fastjson.JSON;

public class JsonUtils {

    public static <K> String toJson(K source){
        return JSON.toJSON(source).toString();
    }


    public static <T> T fromJson(String source, Class<T> clazz){
        return JSON.parseObject(source, clazz);
    }
}
