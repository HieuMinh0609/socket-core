package eln.socketcore.config;


import eln.socketcore.interceptor.LoginInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebMvcConfigSocket implements WebMvcConfigurer {

    /**
     * 视图控制器
     */
//    @Override
//    public void addViewControllers(ViewControllerRegistry registry) {
//        registry.addViewController("/index").setViewName("index");
//        registry.addViewController("/stomp").setViewName("websocket/stomp");
//        registry.addViewController("/chat").setViewName("websocket/chat");
//    }

    /**
     * 添加拦截器
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //registry.addInterceptor(new LoginInterceptor()).addPathPatterns("/user/**","/chat","/sendToUser","/pullUnreadMessage");
        registry.addInterceptor(new LoginInterceptor()).addPathPatterns("/user/save-session");
    }

}